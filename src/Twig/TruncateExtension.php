<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TruncateExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('truncate', [$this, 'truncate']),
        ];
    }

    public function truncate($text, $number)
    {
        $retval = $text;
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $text);
        $string = str_replace("\n", " ", $string);
        $array = explode(" ", $string);
        if (count($array)<=$number)
        {
            $retval = $string;
        }
        else
        {
            array_splice($array, $number);
            $retval = implode(" ", $array)." ...";
        }
        return $retval;

    }

}
