<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class ExpositionSubscriber implements EventSubscriberInterface
{
    public function postSetData(FormEvent $event):void
    {
        /*
         * getData : accès à la saisie du formulaire
         * getForm : accès au builder du formulaire
         * $form->getData() : entité
         */
        $model = $event->getData();
        $form = $event->getForm();
        $entity = $form->getData();
        // modifier les contraintes du champ fichier
        // si l'entité est mise à jour, pas de contraintes
        $constraints = $entity->getId() ? [] : [
            new NotBlank([
                'message' => "du contenu est obligatoire"
            ])
        ];

        //dd($model, $form, $entity);
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::POST_SET_DATA => 'postSetData'
        ];
    }
}
