<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Form\Model\ContactModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact.form")
     */
    public function form(Request $request, Environment $twig):Response
    {
        $type = ContactType::class;
        $model = new ContactModel();

        $form = $this->createForm($type, $model);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //juste un message d'envoi , le formulaire n'envoi rien
            // impossible d'installer l'extension mailer pour l'envoi, a corriger
            $this->addFlash('notice', 'Email envoyé');
            return $this->redirectToRoute('contact.form');
        }


        return $this->render('contact/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

}