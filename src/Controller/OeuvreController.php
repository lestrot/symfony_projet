<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\OeuvreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OeuvreController extends AbstractController
{
    /**
     * @Route("/oeuvres", name="oeuvres.index")
     */
    public function index(OeuvreRepository $oeuvreRepository, CategoryRepository $categoryRepository):Response
    {
        $cat = $categoryRepository->findAll();
        $results = $oeuvreRepository->findAll();

        return $this->render('oeuvres/index.html.twig', [
            'results' => $results,
            'cat' => $cat
        ]);
    }
    /**
     * @Route("/oeuvres/{slug}", name="oeuvres.detail")
     */
    public function detail(OeuvreRepository $oeuvreRepository, string $slug):Response
    {
        $result = $oeuvreRepository->findOneBy([
            'slug' =>$slug
        ]);
        return $this->render('oeuvres/detail.html.twig', [
            'result' => $result
        ]);
    }
}